FROM nginx:latest

RUN apt-get update -y
RUN curl -fsSL https://deb.nodesource.com/setup_17.x | bash -
RUN apt-get install -y nodejs
RUN apt-get install -y fish
RUN apt-get install -y vim

WORKDIR /code
COPY . .
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY .bash_history ~/.bash_history

RUN npm install --prefix /code/app1/
RUN npm install --prefix /code/app2/

WORKDIR /

# ENTRYPOINT ["tail", "-f", "/dev/null"]

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
