const express = require('express');
const app = express();

const PORT = 5000;

app.get('/', (req, res) => {
    res.send('App1: Hello from /!')
})
app.get('/niko', (req, res) => {
    res.send('App1: Hello from /app1!')
})

app.listen(PORT, () => console.log(`Server is up and running on port ${PORT}`));