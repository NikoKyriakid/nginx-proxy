const express = require('express');
const app = express();

const PORT = 5001;

app.get('/', (req, res) => {
    res.send('App2: Hello from /!')
})

app.get('/app2', (req, res) => {
    res.send('App2: Hello from /app2!')
})
app.listen(PORT, () => console.log(`Server is up and running on port ${PORT}`));